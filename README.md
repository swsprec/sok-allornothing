# SoK-AllOrNothing
Data and code for IEEE Euro S&amp;P paper: __SoK: All or Nothing - A Postmortem of Solutions to the Third-Party Script Inclusion Permission Model and a Path Forward__

Link to the paper: [here](https://swsprec.com/papers/SoKPostmortem.pdf)

Authors are available to help out and provide additional documentation as needed. 
